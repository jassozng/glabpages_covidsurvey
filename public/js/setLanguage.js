window.onload = function(){
    let slider = document.getElementById("language");
    let citySelect = document.getElementById("Qtn2");
    let entity = document.getElementById("Qtn1");
    let div = document.getElementById("div-city");
    let data = {
        "Aguascalientes":"Aguascalientes, Asientos, Calvillo, Cosío, Jesús María, Pabellón de Arteaga, Rincón de Romos, San José de Gracia, Tepezalá, El Llano, San Francisco de los Romo",
        "Baja California": "Ensenada, Mexicali, Tecate, Tijuana, Playas de Rosarito, San Quintín",
        "Baja California Sur": "Comondú, Mulegé, La Paz, Los Cabos, Loreto",
        "Campeche": "Calkiní, Campeche, Carmen, Champotón, Hecelchakán, Hopelchén, Palizada, Tenabo, Escárcega, Calakmul, Candelaria, Seybaplaya",
        "Coahuila": "Abasolo, Acuña, Allende, Arteaga, Candela, Castaños, Cuatro Ciénegas, Escobedo, Francisco I. Madero, Frontera, General Cepeda, Guerrero, Hidalgo, Jiménez, Juárez, Lamadrid, Matamoros, Monclova, Morelos, Múzquiz, Nadadores, Nava, Ocampo, Parras, Piedras Negras, Progreso, Ramos Arizpe, Sabinas, Sacramento, Saltillo, San Buenaventura, San Juan de Sabinas, San Pedro, Sierra Mojada, Torreón, Viesca, Villa Unión, Zaragoza"
    };
    entity.addEventListener("change", function(){
        let entityValue = document.getElementById("Qtn1").value;
        let dataArray = data[entityValue].split(",");
        let html = "<label for='Qtn2' class='form-label'>Selecciona el municipio donde vives</label>" + 
                    "<select name='Qtn2' id='Qtn2' class='form-input' required>";
        for (let i = 0; i < dataArray.length; i++) {
            html = html + "<option value="+dataArray[i]+">"+dataArray[i]+"</option>"
        };
        div.innerHTML = html;
    });
    
    slider.addEventListener("change", function(){
        if(slider.checked){
            window.location.href = "./indexEn.html";
        }else{
            window.location.href = "./index.html";
        }
    });
}